import 'package:flutter/material.dart';
import 'package:proyek_todo/style.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: TodoListPage(),
    );
  }
}

class TodoListPage extends StatefulWidget {
  const TodoListPage({
    super.key,
  });

  @override
  State<StatefulWidget> createState() => TodoListState();
}

class TodoListState extends State<TodoListPage> {
  int counter = 0;
  List<String> todoList = [
    'Belajar Flutter',
    'Pelatihan keuangan',
    'Makan malam bersama pacar',
    'Jangan lupa bernafas'
  ];
  TextEditingController todoCtrl = TextEditingController();

  void tampilForm() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Form Todo"),
            content: Container(
              child: TextField(
                controller: todoCtrl,
                decoration: InputDecoration(hintText: "Kerjaannya ..."),
              ),
            ),
            actions: [
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      todoList.add(todoCtrl.text);
                    });

                    todoCtrl.text = '';
                    Navigator.pop(context);
                  },
                  child: Text("Tambah Todo"))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Aplikasi Todo")),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_box),
        onPressed: () {
          tampilForm();
        },
      ),
      body: Column(
        // mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(20),
            child: Text('List Todo',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 28,
                    color: Colors.blue)),
          ),
          Expanded(
              child: ListView.builder(
                  itemCount: todoList.length,
                  itemBuilder: ((context, index) {
                    return ListTile(
                      leading: const Icon(Icons.check_box, color: Colors.green),
                      title: Text(
                        todoList[index],
                        style: header3,
                      ),
                      trailing: IconButton(
                        icon: const Icon(Icons.delete_forever_rounded,
                            color: Colors.red),
                        onPressed: () {
                          setState(() {
                            todoList.removeAt(index);
                          });
                        },
                      ),
                    );
                  })))
        ],
      ),
    );
  }
}
