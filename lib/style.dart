import 'package:flutter/material.dart';

const header1 = TextStyle(fontWeight: FontWeight.bold, fontSize: 30);
const header2 = TextStyle(fontWeight: FontWeight.bold, fontSize: 25);
const header3 = TextStyle(fontWeight: FontWeight.bold, fontSize: 20);
const header4 = TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
